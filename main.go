package main

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/labstack/echo/v4"
	"gitlab.com/jhes/go-skeleton/api/product"
)

func main() {
	db, err := gorm.Open("mysql", "root@tcp(localhost)/ditenun?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	e := echo.New()
	e.HideBanner = true
	e.Debug = true

	product.Register(e, db)

	e.Logger.Fatal(e.Start(":8080"))
}
