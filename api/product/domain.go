package product

import (
	"time"
)

// Product represent product instance
type Product struct {
	ID          int64      `json:"id"`
	Slug        string     `json:"slug"`
	Name        string     `json:"name"`
	Price       float64    `json:"price"`
	Description string     `json:"description"`
	Color       string     `json:"color"`
	Size        string     `json:"size"`
	Stock       int64      `json:"stock"`
	IsActive    int8       `json:"-"`
	CreatedAt   time.Time  `json:"-"`
	UpdatedAt   time.Time  `json:"-"`
	DeletedAt   *time.Time `json:"-"`
}

// Repository represent the product's repository contract
type Repository interface {
	Fetch() ([]Product, error)
	// TODO: add more repository method
}

// Usecase represent the product's usecases
type Usecase interface {
	Fetch() ([]Product, error)
	// TODO: add more usecase method
}
