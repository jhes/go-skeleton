package product

import (
	"github.com/jinzhu/gorm"
)

// mysqlRepository is implementation of Repository using MySQL
type mysqlRepository struct {
	conn *gorm.DB
}

// NewMysqlRepository returns an instance of Repository implementation
// using MySQL
func NewMysqlRepository(conn *gorm.DB) Repository {
	return &mysqlRepository{
		conn: conn,
	}
}

// Fetch returns products from database
func (r *mysqlRepository) Fetch() ([]Product, error) {
	var products []Product

	r.conn.Find(&products, "is_active = 1 AND deleted_at IS NULL")

	return products, nil
}
