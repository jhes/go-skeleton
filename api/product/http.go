package product

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

// HTTPHandler contain HTTP handler for product endpoint
type HTTPHandler struct {
	u Usecase
}

// NewHTTPHandler returns an instance of HTTPHandler
func NewHTTPHandler(e *echo.Echo, u Usecase) {
	h := HTTPHandler{
		u: u,
	}

	e.GET("/products", h.Index)
}

// Index handles GET `/products` endpoints.
// This handler fetch products from database,
// and return into JSON format
func (h HTTPHandler) Index(c echo.Context) error {
	products, _ := h.u.Fetch()

	return c.JSON(http.StatusOK, products)
}
