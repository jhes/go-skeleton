package product

// usecase is implementation of Usecase
type usecase struct {
	r Repository
}

// NewUsecase returns an instance of usecase using
// using given r as repository
func NewUsecase(r Repository) Usecase {
	return &usecase{
		r: r,
	}
}

// Fetch returns products from database
func (u *usecase) Fetch() ([]Product, error) {
	return u.r.Fetch()
}
