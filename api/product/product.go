package product

import (
	"github.com/jinzhu/gorm"
	"github.com/labstack/echo/v4"
)

// Register registers http handler, usecase and repisitory
// of products
func Register(e *echo.Echo, db *gorm.DB) {
	NewHTTPHandler(e,
		NewUsecase(
			NewMysqlRepository(db),
		),
	)
}
