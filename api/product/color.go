package product

const (
	// ColorWhite is an const for white color
	ColorWhite = "white"
	// ColorBlack is an const for black color
	ColorBlack = "black"
	// ColorGray is an const for gray color
	ColorGray = "gray"
)
